# Miniprojets 1

## Sujets possibles

#### Une classe pour gérer les fractions en python

Il n'y a pas par défaut de type pour gérer les fractions en python. Les nombres flottants ont le défaut
d'être approximatifs : pour le vérifier, tester `0.1 + 0.2 == 0.3` dans la console python.

Créez une classe Fraction, dont le constructeur `__init__` prendra deux paramètres, pour qu'on
puisse créer un objet de type Fraction qui représente 3/12 avec : `f = Fraction(3,12)`.

Spécifiez, ajoutez et testez ensuite les méthodes pour gérer les fractions :

* pour multiplier par une autre fraction
* pour ajouter une autre fraction
* pour simplifier la fraction
* pour comparer à une autre fraction
* ...

#### Mastermind interactif

Pour simplifier, on va travailler avec des chiffres plutôt que des couleurs.
Le code secret est un nombre à 5 chiffres, écrit dans votre code python.

La personne qui joue essaye un code jusqu'à trouver.

Puis ajoutez progressivement les fonctionnalités suivante :

* mettre un nombre d'essai maximum avant game over
* faire choisir le code secret au hasard (module random)
* donner comme indice à chaque tour le nombre de chiffres corrects
* donner en plus le nombre de chiffres corrects mais mal placés
* ...

#### Des filtres de transformation d'image

Le but est d'utiliser le module PIL pour faire du traitement d'image.

On veut pouvoir :

* ouvrir un fichier image
* parcourir l'image pixel par pixel, lire les valeurs RGB de la couleur, et les modifier

Lisez la documentation de PIL pour apprendre à faire ça.

Ensuite :

* créez un filtre qui transforme une image en niveaux de gris.
* ...

### Enquêter sur les inégalités avec des données publiques

L'INSEE met à disposition un fichier sur les emplois avec 2.3 millions de lignes,
une pour chaque personne d'un échantillon d'1/12e de la population salariée en France : https://www.insee.fr/fr/statistiques/5366604 .

On ne peut pas traiter un fichier aussi gros dans un tableur, mais on peut le lire en
python avec le module **csv**.

Téléchargez le fichier csv, essayez de comprendre la structure de la table.

Lisez la documentation du module csv.

Puis calculez :

* pour la tranche de salaire la plus haute, combien de femmes et combien d'hommes gagnent un salaire dans cette tranche
* faites le automatiquement sur chaque tranche de salaire, en affichant la proportion de femmes en %
* ... (affichage sous forme de graphique avec `matplotlib`)

#### Débuts d'un jeu avec pygame

On peut reprendre le code qu'on a écrit pour les balles rebondissantes avec pygame.

Puis au choix :

* enlever les rebonds sur le côté du bas, pour que les balles s'échappent, et créer
une barre qui se déplace horizontalement avec les flèches du clavier et sur laquelle
les balles rebondissent (début de casse-brique)
* faire pareil avec deux barres sur les côtés droits et gauches, et 4 touches pour les
contrôler (début du jeu Pong)
* enlever toutes les balles, puis essayer de créer un snake qui se déplace avec les touches
du clavier (d'abord sans gérer les collisions avec sa queue, et sans gérer sa taille non plus)
* enlever les balles, et laisser l'utilisateur dessiner sur le fond noir en cliquant avec la souris
(début d'un logiciel de dessin)

Dans tous les cas, il faudra lire un peu la documentation de pygame.

## Consignes

#### Rendu

Vous devez rendre, par groupe :

* Le ou les fichiers python de votre projet
* Un fichier `README.txt` qui contiendra :
    + un journal de votre progression, de la répartition du travail entre
	vous, des problèmes rencontrés, des solutions trouvées ou non et des choix
	qui ont été faits.
    + les sources utilisées (liens vers les pages internet qui vous ont
	aidées).
    + une présentation succinte de ce que fait votre programme.

#### Planning

Le projet est à rendre pour le samedi 16 octobre.


La semaine suivante, avant les vacances, vous passerez par groupe à l'oral pour
présenter votre projet à la classe (~5min) et répondre à des questions (~5min).

#### Barème

La notation tiendra compte des éléments suivants :

* L'organisation et le travail en groupe (répartition du travail, compréhension
par chacun de ce qui est fait).
* Le processus d'élaboration de votre projet (qui doit être documenté dans le README :
vos objectifs, étapes, problèmes rencontrés et comment vous avez fait).
* La qualité du code, sa documentation (commentaires, spécification des fonctions),
les tests avec des `assert` quand c'est possible.
* Le résultat : est-ce que ça marche, est-ce que c'est intéressant et adapté à ce qui
était possible pour votre groupe ?
* L'oral :
    - la présentation à la classe.
	- la réponse aux questions : savoir expliquer vos choix et votre code.
