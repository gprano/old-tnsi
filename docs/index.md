# Au programme du bac écrit

Sont **exclus** du programme des épreuves écrites : les graphes, les SGBD (systèmes de gestion de base de données),
la cryptographie, la programmation fonctionnelle, les notions de calculabilité/décidabilité, et en algorithmique
les algos de recherche de texte (boyer-moore), la programmation dynamique, et les algos sur les graphes.

Il faut donc maîtriser tout le reste des programmes de NSI de 1ère et Terminale, dont voici les principaux thèmes :

* la programmation python : boucles, fonctions, variables, listes, dictionnaires, programmation orientée objet, récursivité,
  tests avec assert,...
* les structures de données : files, piles, arbres. Parcours d'arbres en profondeur et en largeur.
* en algorithmique : tris (par insertion, par sélection, tri fusion), recherche par dichotomie, kNN,
  algorithmes récursifs, principe de "diviser pour régner", principe des algorithmes gloutons, trouver
  la complexité d'un algorithme.
* le modèle relationnel et les bases de données, les requêtes en SQL.
* les bases de systèmes d'exploitation, de ligne de commande linux, l'ordonnancement des processus,
[système sur puce](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.1_Systemes_sur_puce/cours/).
* les notions de réseaux et d'adresses IP, de sous-réseau, de routage avec RIP et OSPF.


# Contenu du site

## Corrigé d'activités sur capytale

* Reprise en main en python : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/289b-36553) et [Corrigé](notebooks/reprise_en_main.ipynb).

* Les listes : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/3bf8-94726) et [Corrigé](notebooks/listes.ipynb).

* Les dictionnaires : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/9f7a-260067) et [Corrigé](notebooks/dictionnaire.ipynb).

* Piles et Files en programmation orientée objet : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/b87b-55796) et
[Corrigé](notebooks/corrige_piles_files.ipynb).

* Les listes chaînées et les arbres en programmation orientée objet : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/1419-92125) et 
[Corrigé](notebooks/listes_arbres.ipynb).

* Les parcours d'arbres (en profondeur, en largeur) : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/da20-106345)
et [Corrigé](notebooks/parcours_arbres.ipynb).

* Programmation Récursive : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/b26b-68854) et [Corrigé](notebooks/programmation_recursive.ipynb).

* Implémentation de la mise à jour dans le protocole de routage RIP : [Lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/593b-263728)
et [Corrigé](notebooks/protocole_rip.ipynb).


## Programmation en python

* [Résumé](notebooks/python_base) des bases du langage et [cours en pdf](pdf/Cours_programmation.pdf) de M.Passieux.

* [Conseils](python/advent) pour participer à Advent Of Code : lecture de fichier, manipulation de chaînes de caractères.

* [Programmation Orientée Objet](poo)
  [Cours](https://glassus.github.io/terminale_nsi/T2_Programmation/2.1_Programmation_Orientee_Objet/cours/) et [TP sur les balles rebondissantes](https://glassus.github.io/terminale_nsi/T2_Programmation/2.1_Programmation_Orientee_Objet/TP/) de G.Lassus.
  Solutions du TP [sans les collisions](python/balles_simple.py) et [avec les collisions](python/balles_collisions.py).

## Algorithmique

* [Algorithmes Gloutons](algo/gloutons) et [sujet du TP](pdf/gloutons.pdf) fait en cours.

* [Complexité](algo/complexite) : coût des algorithmes en fonction de la taille du problème à résoudre.

* [Dichotomie](algo/dichotomie) : recherche rapide dans un tableau trié.

* [Tris](algo/tris) : par sélection et par insertion : complexité quadratique, et tri fusion : complexité quasi-linéaire.

* [TP diviser pour mieux régner](seances/tp_pillow.md) avec un algorithme récursif de rotation d'image.

* [TP sur l'algorithme d'apprentissage kNN](algo/knn.md)

* [Algorithmique du texte](algo/recherche_texte.md) : recherche de sous-chaîne avec Boyer-Moore-Horspool.

## Structures de Données

* Implémentation des piles et des files (voir sujet et corrigé de l'activité capytale plus haut).

* [Résumé](structures/arbres) de cours sur les arbres.

* Voir les activités capytale pour l'implémentation des arbres en POO, et les parcours d'arbres.

* Graphes : [TP sur le parcours en profondeur](seances/tp_parcours_profondeur_graphes.zip) (fichier zip) et [suite du sujet](SUITE_SUJET.txt).  
  [Fin du sujet](FIN_SUJET.txt) sur le parcours en largeur.
  [Correction](algo/parcours_graphes).
  
## Systèmes d'Exploitation, Réseau, Sécurité

* [Premier TP](os/tp1.md), fait avec des versions de linux portables installées sur clé usb.

* [Deuxième TP](os/tp2.md) et [lien vers un cours](https://pixees.fr/informatiquelycee/term/c19c.html).

* [Lien vers un cours](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.3_Protocoles_de_routage/cours/) sur
  le routage, et les protocoles RIP et OSPF.
  
* [Troisième TP](os/tp3.md) sur les permissions de fichiers/dossiers.

* [Lien](https://glassus.github.io/terminale_nsi/T5_Architecture_materielle/5.4_Cryptographie/cours/) vers un cours de G.Lassus sur la cryptographie.

* :new: [TP sur la cryptographie](os/tp_cryptographie.md).

* :new: [TP sur la stéganographie](os/tp_steganographie.md).


## Bases de données

* [Lien vers un cours](https://glassus.github.io/terminale_nsi/T4_Bases_de_donnees/4.1_Modele_relationnel/cours/) sur le modèle relationnel.
* [Cours](bdd/sql) pour les requêtes SQL sur une seule table.
* [Solution](bdd/murder_solution) de l'enquête SQL Murder Mystery en utilisant des JOIN (requêtes sur plusieurs tables).
* Lien vers les trois activités SQL sur les BDD (base d'élèves de lycée):
    1. Notions générales et recherche de données dans une table : [lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/475e-223056).
    2. Édition des données [lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/73a6-232932).
    3. Jointures [lien capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/0adb-232988).
* [Corrigé et sujet](bdd/ds.md) du ds

## Autres

* [Corrigé](corriges/devoir1.md) du premier devoir de 2h.

* Le manuel de NSI qui vous a été fourni est très détaillé et rigoureux, n'hésitez pas à vous en servir
  si vous voulez un autre point de vue ou un complément. Il contient aussi des exercices corrigés.

* [Lien](https://pixees.fr/informatiquelycee/term/suj_bac/index.html) vers une liste de sujets et corrigés de l'épreuve écrite.
