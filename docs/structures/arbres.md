# Les arbres (en informatique)

Un arbre est une structure de donnée hiérarchique (par étages) et récursive.

![exemple d'arbre](arbre1.svg)

## Vocabulaire :

* un arbre est composé de **noeuds** qui peuvent contenir des données (leur **valeur** ou **étiquette**).
* un noeud peut-être relié à un noeud au dessus de lui, son **parent**. Les noeuds d'un arbre ont tous
un parent, sauf la **racine** qui n'en a aucun.
* un noeud peut être relié à des noeuds en dessous qui sont ses **enfants**. S'il n'en a aucun, on dit que
c'est une **feuille** de l'arbre.
* la **taille** d'un arbre est le nombre de noeud qu'il contient. Sa **hauteur** est le nombre d'étages (il faut
préciser si on choisit que l'arbre qui n'a qu'une racine est de hauteur 1 ou 0 : on prendra 1 dans ce cours,
et 0 pour la hauteur d'un arbre vide).
* **arbre binaire** : un arbre est dit binaire si tous ses noeuds ont au plus deux enfants, et qu'on distingue alors
l'enfant gauche et l'enfant droit.

## Encadrement de la hauteur

Certains algorithmes auront une complexité qui dépend non pas de la taille de l'arbre, mais de sa hauteur.

Pour un arbre de taille `n`, on peut donc se demander quelle est la hauteur `h` minimale et maximale possible.

* la hauteur maximale est égale à `n` si chaque noeud n'a qu'un enfant, sauf l'unique feuille.

* la hauteur minimale est 2 si l'arbre est composé d'une racine qui a `n-1` enfants.

* si l'arbre est binaire, c'est plus compliqué. On peut montrer que comme la taille peut environ doubler à
chaque fois que la hauteur augmente de 1, si l'arbre est _complet_ alors la hauteur sera d'environ `log(n)`.


Pour un arbre binaire on a donc `log(n) <= h <= n`.

Si la hauteur est de l'ordre de `log(n)`, on dira que l'arbre est _équilibré_.

## Définition récursive

On peut définir les arbres de manière récursive. Un arbre est :

 * Soit un arbre vide (aucun noeud)
 * Soit une racine, reliée à une ou plusieurs structures qui sont
   aussi des arbres : on dit que ce sont des _sous-arbres_.
   
Dans le cas des arbres binaires, on distingue le sous-arbre droit et
le sous-arbre gauche.

Grâce à cette définition, il va être naturel de résoudre les problème
sur les arbres de manière récursive. Par exemple pour trouver la hauteur
d'un arbre :

* Si l'arbre est vide, c'est 0.
* Sinon, c'est 1 plus le maximum de la hauteur de ses sous-arbres.

## Arbres binaires de recherche

Les ABR sont des arbres binaires qui vérifient la propriété suivante : 

La valeur de chaque noeud est plus grande que celle de tous les noeuds
contenus dans son sous-arbre gauche, et plus petite que celle
de tous les noeuds contenus dans son sous-arbre droit.

Cela permet de rechercher si une valeur est dans l'arbre de la manière suivante :

* Si la valeur est celle de la racine, on l'a trouvée.
* Si elle est plus petite que celle de la racine, il faut chercher dans le sous-arbre gauche.
* Si elle est plus grande, il faut chercher dans le sous-arbre droit.

Ainsi on descend dans l'arbre, et le nombre d'étapes est proportionnel à la hauteur de l'arbre.

On peut aussi insérer une valeur en descendant l'arbre comme pour la rechercher, et dès
qu'on arrive sur un sous-arbre vide, on l'insère à la place.

Il est aussi possible de supprimer un noeud avec une complexité `O(h)` mais c'est un petit
peu plus compliqué et pas au programme de NSI (explication p166 de votre livre).

L'efficacité des opérations sur un ABR dépend donc de sa hauteur : s'il est équilibré, elle seront
de complexité logarithmique. C'est ce qu'il se passe en moyenne quand les valeurs sont insérées dans
un ordre aléatoire. Si par contre les valeurs sont insérées à peu près dans l'ordre, la hauteur sera
de l'ordre de grandeur de la taille et la complexité sera linéaire.

Pour avoir une structure de donnée qui permet de gérer efficacement un _ensemble_ (pouvoir ajouter, supprimer
un élément et tester si un élément est dedans avec une complexité logarithmique), on peut modifier les
opérations pour qu'elles rééquilibrent l'arbre automatiquement au fur et à mesure : c'est le cas pour les
arbres "rouge-noir" (hors programme). Les dictionnaires de python pourraient être implémentés comme cela,
mais en fait ils utilisent une autre technique.
