## Installer python et htop

La plupart des versions de linux utilisent un gestionnaire de paquet
pour installer des logiciels. Cela permet d'installer des logiciels
à partir de sources de confiance, et de gérer automatiquement les
dépendances, et les mises à jour.

Essayez de lancer la commande 'apt install python' pour installer python.

Quelle erreur obtenez-vous ?

Rajoutez "sudo" au début : 'sudo apt install python' pour exécuter la
commande avec les privilèges du super-utilisateur (normalement, on a
besoin de rentrer un mot de passer pour ça).

Puis 'sudo apt install htop'.

## Processus, pid, ppid, commandes top et kill

Chaque processus est identifié par un numéro appelé PID.

La commande 'top' permet de voir une liste des processus en cours,
la commande 'htop' donne une vue plus jolie et un peu différente.
(tapez 'q' pour quitter ces visualisations)

Dans un terminal, lancer la commande 'sleep 1000' qui va juste attendre 1000s.
Dans un autre, lancer htop et chercher le programme sleep (vous pouvez faire
une recherche en tapant '/' puis le texte recherché).
Quel est son PID ? Utilise-t-il beaucoup de Mémoire/CPU ?

On peut envoyer un signal pour arrêter un processus avec la commande 'kill PID'.
Arrêtez ainsi le programme sleep, et vérifiez qu'il s'est bien terminé dans le
terminal où vous l'avez lancé.

Souvent, des programmes démarrent d'autres programmes. On dit alors que le
nouveau programme est l'enfant de celui qui l'a créé, et on a à nouveau une
structure d'arbre. Utilisez la commande 'pstree' pour visualiser l'arbre des
processus en cours. Quel est le processus à la racine ?

Dans le terminal, on peut avoir des variables d'environnement, et on met $ devant
pour y accéder. Si un processus accède à $PPID, la variable contiendra le PID de
son processus parent (PPID = parent process id).
Lancez la commande 'echo $PPID'. echo est une commande qui affiche juste le message
dans le terminal (un peu comme le ferait print en python). À votre avis, quel est
le processus parent du programme echo que vous avez lancé ?
Vérifiez en arrêtant avec kill le processus dont vous avez récupéré l'ID.

## Observer python avec htop

Lancez la commande 'python' dans un terminal, qui ouvre une console python.

Dans un autre terminal à côté, lancez htop et cherchez python (avec '/' puis tapez python)
pour surligner la ligne qui correspond à python.

Exécutez les lignes de code suivantes en observant à chaque fois la consommation de mémoire de python :

L = [0] * 10**4

L = [0] * 10**6

L = [0] * 10**7

L = [0] * 10**8

([0] * x crée une liste de x zéros)

Puis les commandes suivantes en observant la consommation de CPU de python:

n=0
for i in range(10**4): n+=1

for i in range(10**6): n+=1

for i in range(10**7): n+=1

for i in range(10**8): n+=1

(on a le droit de mettre le n+=1 sur la même ligne, mais il faut appuyer deux fois sur entrée dans la console)

Combien de coeurs du processeur python utilise-t-il ?

## Programmation concurrente en python

On va utiliser python pour lancer plusieurs threads, des processus "léger" avec de la
mémoire partagée. Lisez le code dans compteur_v1.py.
À votre avis, quelle devrait être la valeur de la variable compteur à la fin ?
(le mot-clé global dans les fonctions permet aux fonctions de changer une variable
qui n'est pas définie dans la fonction).

Lancez-le avec 'python compteur_v1.py' (c'est comme ça qu'on exécute un fichier python 
depuis la ligne de commande). Relancez-le pour vérifier.

Comment expliquer ça ?

Pour mieux comprendre, ouvrez une console python et tapez successivement :

import dis
code = compile("compteur = compteur + 1","","exec")
dis.dis(code)

Cela permet de voir que fait python en interne pour exécuter la ligne "compteur = compteur + 1".

(me demander si vous n'avez pas compris)


Une manière de résoudre le problème est d'utiliser des "verrous". Si un thread prend le verrou
partagé, l'autre ne peut plus le prendre et doit attendre qu'il soit libéré. Cela permet
qu'il n'y ait pas d'entrelacement des exécutions des deux threads dans certaines parties qui sont
critiques, mais ce sera un peu plus lent.

Lisez le code de compteur_v2.py, et testez le.

## Interblocage (deadlock)

Les verrous créent aussi des problèmes.

Supposons qu'on aie un thread 1 qui fasse :

Prendre le verrou A
Prendre le verrou B
Faire des opérations critiques
Libérer le verrou B
Libérer le verrou A

et un thread 2 qui fasse :

Prendre le verrou B
Prendre le verrou A
Faire des opérations critiques
Libérer le verrou A
Libérer le verrou B

Trouvez une manière d'entrelacer ces exécutions qui bloque les deux
threads. On appelle cela un interblocage (deadlock).

Si vous aimez bien trouver des deadlocks, vous pouvez essayer ce jeu (en anglais):

https://deadlockempire.github.io



