# TP Processus et Système d'Exploitation

## 1) Les verrous

Les **verrous** sont fournis par le système d'exploitation et permettent de s'assurer
que des processus ou des threads ne font pas certaines choses en même temps. Par exemple
accéder et modifier de la mémoire partagée.

Le module `threading` de python permet de créer un verrou avec `Lock()`. Cela crée un objet
qui possède une méthode `acquire` pour prendre le verrou et `release` pour le libérer. Le système
d'exploitation s'assure qu'un seul thread peut prendre le verrou à la fois, et un thread qui veut
prendre un verrou déjà pris est mis en attente.

**Exercice (créer un interblocage)**

Recopiez dans Thonny le programme suivant, qui crée et exécute deux threads, un qui exécute la fonction
programme1 et l'autre la fonction programme2.

Complétez ce programme pour que chacune des fonctions prenne les deux verrous juste avant chaque print, et les libère
juste après, en faisant en sorte que l'ordre dans lequel les verrous sont pris soit différent entre les deux threads.

Exécutez le programme. Que se passe-t-il ?

Changez ensuite l'ordre dans lequel les verrous sont pris et réessayez.

```python
from threading import Thread,Lock

verrouA = Lock()
verrouB = Lock()

def programme1():
    for i in range(1000):
        print("truc important du thread 1 qui a besoin des verrous A et B")

def programme2():
    for i in range(1000):
        print("truc important du thread 2 qui a besoin des verrous A et B")

    
# Création des threads
t1 = Thread(target=programme1)
t2 = Thread(target=programme2)

# Lancement des threads
t1.start()
t2.start()

# Attente de la fin d'exécution
t1.join()
t2.join()

print("C'est fini!")
```

## 2) Exécuter des programmes Python et C

Ouvrez une machine virtuelle linux avec le site Katacoda [à cette adresse](https://www.katacoda.com/courses/ubuntu/playground).

### en python

Créez le programme python suivant. Vous pouvez utiliser `nano FICHIER` pour créer et ouvrir FICHIER dans l'éditeur nano. Tapez
Ctrl+O pour sauvegarder (et Entrée pour confirmer le nom du fichier), et Ctrl+X pour quitter nano. Appelez le fichier `puissance.py`.

```python
n = 1

for i in range(100):
    print("i vaut",i,"n vaut",n)
    n = n*2
```

Pour exécuter le programme python contenu dans FICHIER en ligne de commande, il suffit de taper `python3 FICHIER`. `python` ou `python3`
est le nom du programme, qui prend comme argument le fichier de code python qu'il va exécuter. Comme on a toujours besoin du
programme python pour exécuter nos programmes, on dit que c'est un langage **interprété** (et le programme python est l'interpréteur).

Testez le programme python de cette manière. Que fait-il ?

Savez-vous avec quel langage de programmation a été écrit le programme `python` lui-même ?

### en C

Créer un fichier `puissance.c` de la même manière, en copiant ce code dedans :

```C
#include<stdio.h>

int main()
{
  int n = 1;
  for(int i=0;i<100;i++)
    {
      printf("i=%d et n=%d\n",i,n);
      n = n * 2;
    }
}
```

Pour l'exécuter, on a besoin de deux étapes: 

* D'abord la **compilation**. Entrez la commande `gcc puissance.c -o puissance`. Vérifiez avec `ls` qu'un fichier `puissance` a
été créé. Le compilateur `gcc` a transformé le code C en un programme en langage machine. Vous pouvez essayer d'afficher son
contenu avec `cat puissance` mais c'est en binaire et non en texte. La commande `file puissance` permet d'avoir quelques informations dessus.
* Ensuite l'**exécution**. Si on est placé dans le même dossier qu'un programme, on peut l'exécuter simplement avec `./puissance` (`.` est un raccourci
pour le dossier actuel).

Que fait ce programme ? Pourquoi est-ce qu'on n'obtient pas le même résultat que le programme python ?

---

Créez ensuite le programme suivant. `int tableau[10]` crée un tableau de 10 éléments de type int.
Que pensez-vous qu'il va se passer ? Testez.

```C
#include<stdio.h>

int tableau[10];

int main()
{
  for(int i=0;i<20;i++)
      printf("%d\n",tableau[i]);
}
```


## 3) La création de processus avec fork()

Toujours dans la machine virtuelle de Katacoda, copiez et testez ce programme.

```python
import os

resultat = os.fork()

print("fork() a renvoyé la valeur",resultat)
```

Que se passe-t-il ? Que fait la fonction fork, et qu'est-ce qu'elle renvoie ?

**Exercice** : changez ce programme pour que le processus pour lequel fork() a renvoyé 0 affiche "Je suis l'enfant", et l'autre "Je suis le parent".

## 4) Forkbomb !

Voici une célèbre ligne de code que vous pouvez tester sur Katacoda. Que se passe-t-il ?
```bash
:(){ :|:& };:
```

!!! warning "Attention"

	Il est dangereux de recopier des lignes de code lues sur internet dans le terminal de votre machine sans savoir ce qu'elles font...

Le même code en un peu plus lisible :

```bash
f()
{ f | f & };
f
```
En bash (le langage de programmation qu'on peut utiliser dans la ligne de commande) :

* `|` sert à rediriger la sortie d'une commande vers l'entrée d'une autre (on l'apelle "pipe", tuyau en anglais)
* `&` sert à exécuter le programme en arrière-plan

## 5) Deadlock Empire

Essayez les premiers niveaux de ce jeu. 

C'est en anglais, mais le principe est toujours le même.
Vous jouez le rôle de l'ordonnanceur du système d'exploitation, et vous avez plusieurs threads à exécuter.

Vous pouvez avancer chaque thread étape par étape dans l'ordre que vous voulez, votre but est de faire échouer
le programme (bref, de trouver comment un bug peut se passer) :

* soit si deux threads arrivent dans une section critique en même temps, alors qu'un seul devrait pouvoir y accéder à la fois.

* soit par un interblocage.

[https://deadlockempire.github.io](https://deadlockempire.github.io).

