# Stéganographie : l'art de <s>chiffrer</s> dissimuler des informations

On a vu l'importance en sécurité informatique de réfléchir au **modèle de menace**:

![xkcd 538](security.png)

Par exemple, rendre une information impossible à déchiffrer
n'est pas toujours la solution si votre adversaire peut :

* obtenir la clé par la force
* vous empêcher de communiquer (ne pas laisser passer le traffic internet chiffré par exemple)

D'autres techniques peuvent alors être utile, dont la **stéganographie** qui consiste
à cacher une information pour que l'adversaire ne sache pas qu'elle existe. 

Ces techniques dépendent du fait que l'adversaire ne connaît pas la méthode utilisée,
contrairement à la cryptographie où la technique de chiffrement n'est en général pas secrète.

Il y a donc moins d'analyse mathématique des méthodes, qui relèvent plus de l'astuce.

## Exemple : cacher des données dans une image

Il est plus facile de cacher des données dans une image ou une vidéo que dans du texte,
parce qu'il y a beaucoup de données qu'on peut un peu modifier sans que ce soit visible
dans le résultat.

Par exemple, si la couleur d'un pixel est stockée sur 3 octets en RGB (chacune des trois
valeurs entre 0 et 255 pour le rouge, vert et bleu), et qu'on change la quantité de bleu
d'un pixel de 120 à 121, la différence ne sera pas visible à l'oeil nu. On peut se servir
de ça pour cacher de l'information.

Cette image contient deux images en noir et blanc cachées de cette manière :

![pomme](pomme.png)

## Rappel d'utilisation de la bibliothèque PIL pour les images

Ouverture d'un fichier d'image :

```python
from PIL import Image

image = Image.open("pomme.png")
hauteur,largeur = image.size
```

On peut ensuite utiliser :

* `r,g,b = image.getpixel((i,j))` pour obtenir la valeur du pixel aux coordonnées i,j (on obtient un triplet si l'image est en RGB)
* `image.putpixel((i,j),(r,g,b))` pour changer la valeur du pixel aux coordonnées i,j en mettant la couleur (r,g,b)
* `image2 = Image.new("RGB",(hauteur,largeur))` pour créer un nouvel objet d'image en RGB avec les dimensions `hauteur,largeur`
* `image.show()` pour ouvrir une fenêtre qui affiche l'image

## Première image

La première image est cachée dans le bit de poids faible du bleu : si la valeur de bleu est paire 
le pixel caché est noir, sinon il est blanc.

* Créez une nouvelle image aux bonnes dimensions pour écrire l'image secrète
* Pour chaque pixel (donc avec deux boucles for imbriquées pour parcourir tous les pixels (i,j)) :
    * récupérer la valeur du pixel
    * si la valeur de bleu est paire écrire un pixel tout noir dans l'image secrète
    * sinon écrire un pixel tout blanc
* Affichez l'image secrète obtenue

## Deuxième image

La deuxième image est caché dans le bit de poids faible du vert, mais codée différemment : on a mis un
zéro si le pixel est le même que celui juste avant à gauche, et un un sinon.

Retrouvez cette deuxième image.
