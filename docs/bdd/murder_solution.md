# SQL Murder Mystery -- Corrigé

[Lien](https://mystery.knightlab.com/) vers l'enquête.

Correction en utilisant **JOIN** pour obtenir chaque information en une seule requête.
On peut faire sans mais il faut alors faire plusieurs requêtes en réutilisant des résultats intermédiaires.

## Schéma des tables disponibles :

![schema](schema.png)

### Lire le rapport sur le crime (un **meurtre** qui a lieu le **15 jan. 2018** à **SQL City**)

```sql
SELECT description FROM crime_scene_report
WHERE city = 'SQL City' AND type = 'murder' AND date=20180115
```

![réponse 1](enquete1.png)

### Lire l'interview du 1er témoin

```sql
SELECT transcript FROM interview
JOIN person ON interview.person_id = person.id
WHERE address_street_name = 'Northwestern Dr'
ORDER BY address_number DESC
LIMIT 1
```

![réponse 2](enquete2.png)

### Lire l'interview du 2e témoin

```sql
SELECT transcript FROM interview
JOIN person ON interview.person_id = person.id
WHERE address_street_name = 'Franklin Ave'
AND name LIKE 'Annabel%'
```

![réponse 3](enquete3.png)

### Enquête sur les infos du 1er témoin

```sql
SELECT person.name FROM person
JOIN get_fit_now_member AS gym ON gym.person_id = person.id
JOIN drivers_license AS permis ON permis.id = person.license_id
WHERE gym.id LIKE '48Z%' AND permis.plate_number LIKE '%H42W%'
```

![réponse 4](enquete4.png)

### Enquête sur les infos du 2e témoin

À quelle heure Annabel était à la salle de sport le 9 janvier ?

```sql
SELECT check_in_time,check_out_time FROM get_fit_now_check_in AS checkin
JOIN get_fit_now_member AS gym ON gym.id = checkin.membership_id
WHERE name LIKE 'Annabel%' AND check_in_date=20180109
```

![réponse 5](enquete5.png)

Qui d'autre y était à ce moment là ?

```sql
SELECT name FROM get_fit_now_member AS gym
JOIN get_fit_now_check_in AS checkin ON checkin.membership_id = gym.id
WHERE check_in_date=20180109 AND check_in_time <= 1700 AND check_out_time >= 1600
```

![réponse 6](enquete6.png)

### Qui a commandité le crime ?

Lire l'interview du meurtrier :

```sql
SELECT transcript FROM interview
JOIN person ON person.id = interview.person_id
WHERE name='Jeremy Bowers'
```

![réponse 7](enquete7.png)

Trouver la personne avec toutes ces infos, en une requête (et on vérifie qu'elle est bien riche):

```sql
SELECT person.name,car_make,car_model,height,event_name,date,annual_income 
FROM person

JOIN drivers_license AS permis ON person.license_id = permis.id
JOIN facebook_event_checkin AS fb ON fb.person_id = person.id
JOIN income ON person.ssn = income.ssn

WHERE hair_color='red' AND car_make = 'Tesla' AND car_model = 'Model S'
AND height >= 65 AND height <= 67 
AND event_name LIKE 'SQL%'
```

![réponse 8](enquete8.png)
