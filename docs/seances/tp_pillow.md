# TP diviser pour mieux régner : rotation d'image


## La bibliothèque Pillow

```python
from PIL import Image
```

Si Pillow n'est pas installé sur Thonny, allez dans "Outils -> Gérer les paquets", cherchez "Pillow" et installez-le.


```python
img = Image.new("RGB", (largeur, hauteur))
img_2 = Image.open("nom_du_fichier.extension")
```

`new` permet de créer une image vide et `open` d'en ouvrir une (si le fichier est au bon endroit).

Dans les deux cas la valeur de retour est un _objet_ d'une classe spécifique à PIL qui contient
et permet de manipuler l'image. Voici quelques méthodes dont on va se servir :

```python
largeur, hauteur = img.size
```

`img.size` est un attribut qui contient le couple (hauteur, largeur) de l'image.

```python
couleur = img.getpixel((x,y))
```

Attention, `getpixel` prend les coordonnées du pixel à récupérer sous forme d'un tuple.
Le pixel en haut à gauche de l'image a les coordonnées (0,0).

Si l'image est en RGB, cette fonction renvoie également un tuple : les valeurs du rouge, vert et
bleu pour ce pixel.

```python
img.putpixel((x,y), (r,g,b))
```
Change la couleur du pixel.

```python
img.save("nom_du_fichier.extension")
```

Vous avez deviné.

```python
img.show()
```

Ouvre directement l'image dans un logiciel de visualisation.

## Démarrage

On va travailler sur cette image:

![dragon](../images/dragon.png)

!!! question
	Quelle est la couleur du pixel de coordonnées (230,300) ?
	
	??? solution "spoiler"
		(233,58,55) soit 233 de rouge, 58 de vert et 55 de bleu
		
		En hexadécimal c'est le code couleur #E93A37 car 233=E9, 58=3A, 55=37
		<div style="width:25px;height:25px;background-color:rgb(233,58,55)"></div>

!!! question
	Combien y a-t-il de pixels complètement noirs sur l'image ? De pixels complètement blancs ?
	
	??? solution "spoiler"
		292 pixels noirs, 18 pixels blancs
		
		
## Algorithme de rotation récursif

On veut tourner l'image d'un quart de tour vers la droite.

L'idée de l'algorithme récursif est la suivante :

* Si l'image n'est pas réduite à un seul pixel, on va la couper en 4
comme ceci:
```
A | B
-----
C | D
```
* On va récursivement faire la rotation des sous-images A,B,C et D.
* Enfin on va échanger les sous-images de place comme suit : `A->B->D->C->A`
  (mais sans avoir besoin de les faire tourner).


!!! question
	Si on ne sait qu'échanger deux sous-images de place, comment peut-on faire
	la dernière étape avec plusieurs échanges ? Dessiner la position des images
	après chaque étape.

!!! question
	Quelle doit être la condition sur l'entier `n` pour que l'algorithme fonctionne ?

## Échange de sous-images

Programmez d'abord une fonction `echange_pixel` qui prend 5 paramètres:

* `image` une image sous forme d'un objet de Pillow
* `x0` la colonne du premier pixel
* `y0` la ligne du premier pixel
* `x1` la colonne du deuxième pixel
* `y1` la colonne du deuxième pixel

et qui échange les couleurs des pixels (x0,y0) et (x1,y1).

??? note "Aide si vous n'y arrivez pas"
	```python
	def echange_pixel(image,x0,y0,x1,y1):
		#votre code ici
	```
	Il va falloir utiliser deux fois la méthode getpixel,
	puis deux fois la méthode putpixel.

Puis programmez une fonction `echange_images` qui prend 6 paramètres:

* `image`
* `x0` et `y0` les coordonnées du coin en haut à gauche de la première sous-image
* `x1` et `y1` idem pour la deuxième sous-image
* `n` la taille des sous-images carrées à échanger.

qui échange deux sous-images d'une image. Utilisez la fonction `echange_pixel` que
vous venez de créer.

??? note "Aide si vous n'y arrivez pas"
	```python
	def echange_images(image,x0,y0,x1,y1,n):
		#votre code ici
	```	
	Il faut parcourir tous les pixels de l'image avec deux
	boucles for imbriquées, et utiliser la fonction `echange_pixel` à chaque fois.

Testez votre fonction sur l'image de dragon.

## Rotation

Programmez maintenant la fonction récursive `rotation` qui prend 4 paramètres:

* `image`
* `x0` et `y0` les coordonnées du coin en haut à gauche de la partie à tourner
* `n` la taille de l'image nxn à tourner

Testez bien sûr le résultat sur l'image de dragon.

## Visualisation de l'algorithme

Cet algorithme n'est pas très efficace par rapport à l'algorithme plus simple
qui consiste à créer une nouvelle image de la bonne taille, et à copier chaque
pixel au bon endroit dans la nouvelle image.

Il a l'intérêt d'être un algorithme **en-place**, c'est à dire qu'on n'a pas 
besoin de créer une nouvelle image et qu'on n'utilise donc presque pas de mémoire
en plus de celle pour stocker l'image initiale. Mais cet intérêt est limité, en
pratique on ne fera sans doute jamais ça.

Par contre il est joli à visualiser 🤩

1. Initialisez une liste vide au début du programme (dans une variable `frames`)
2. À chaque appel de la fonction rotation si n est supérieur ou égal à 32, ajoutez à la fin de la liste `frames` une copie de l'image actuelle (avec `image.copy()`)
3. Après avoir fait la rotation, sauvegardez le résultat au format GIF comme ceci :
```python
image.save("animation.gif", format="GIF", append_images=frames,
        save_all=True, duration=200, loop=0)
```		
4. Allez voir le résultat !


!!! note "Extension 1"
	Si on veut visualiser les étapes plus profondes de la récursion, on a un problème de taille d'image car il y a beaucoup d'étapes à stocker. Écrivez une fonction qui divise la taille d'une image par deux en faisant la moyenne des quatre pixels voisins pour créer les pixels de l'image réduite. Puis visualisez une étape de plus dans la récursion.
	
!!! note "Extension 2"
	Programmez l'algorithme "normal" de rotation sans récursion
	
!!! note "Extension 3"
	Trouvez une manière de faire l'algorithme "normal" sans récursion et
	cette fois _sans créer une nouvelle image_, donc en utilisant une mémoire
	supplémentaire constante.
