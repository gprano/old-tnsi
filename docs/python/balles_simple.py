import pygame
import time
import random
#
noir = (0,0,0)
couleur = (255,0,0)
pygame.init()
fenetre = pygame.display.set_mode((640, 480))
fenetre.fill(noir)
#
class Balle:
    def __init__(self):
        self.x = random.randint(10,640-10)
        self.y = random.randint(10,480-10)
        self.dx = random.randint(-5,5)
        self.dy = random.randint(-5,5)
    def draw(self):
        pygame.draw.circle(fenetre,couleur,(self.x,self.y),10)
    def move(self):
        self.x += self.dx
        self.y += self.dy
        if self.x <= 0 or self.x >= 640:
            self.dx = -self.dx
        if self.y <= 0 or self.y >= 480:
            self.dy = -self.dy
#
L = []
for i in range(50):
    b = Balle()
    L.append(b)
#
running = True
while running:
    fenetre.fill(noir)
    for b in L:
        b.move()
        b.draw()

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    time.sleep(0.005)
#
pygame.quit()
