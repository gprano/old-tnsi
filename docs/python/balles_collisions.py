import pygame
import time
import random
#
noir = (0,0,0)
#
pygame.init()
fenetre = pygame.display.set_mode((640, 480))
fenetre.fill(noir)
#
def dist(x1,y1,x2,y2):
    return ((x1-x2)**2 + (y1-y2)**2)**0.5
#
def random_color():
    return (random.randint(0,255),random.randint(0,255),random.randint(0,255))
#
class Ball:
    def __init__(self):
        self.couleur = random_color()
        self.x = random.randint(10,640-10)
        self.y = random.randint(10,480-10)
        self.dx = random.randint(-5,5)
        self.dy = random.randint(-5,5)
        self.rayon = random.randint(2,15)
    def draw(self):
        pygame.draw.circle(fenetre,self.couleur,(self.x,self.y),self.rayon)
    def move(self):
        self.x += self.dx
        self.y += self.dy
        if self.x <= self.rayon:
            self.dx = abs(self.dx)
        if self.x >= 640-self.rayon:
            self.dx = -abs(self.dx)
        if self.y <= self.rayon:
            self.dy = abs(self.dy)
        if self.y >= 480- self.rayon:
            self.dy = -abs(self.dy)
    def collision(self,other):
        return dist(self.x,self.y,other.x,other.y)<self.rayon+other.rayon
#
NB = 50
balls = [Ball() for i in range(NB)]
collision = [[False for _ in range(NB)] for _ in range(NB)]
#
running = True
while running:
    fenetre.fill(noir)
    for b in balls:
        b.move()
    for b in balls:
        b.draw()
    # collisions
    n = len(balls)

    new_collision = [[False for _ in range(NB)] for _ in range(NB)]
    for i in range(n):
        for j in range(i):
            new_collision[i][j] = balls[i].collision(balls[j])
            if not collision[i][j] and new_collision[i][j]:
                balls[i].dx,balls[j].dx = balls[j].dx,balls[i].dx
                balls[i].dy,balls[j].dy = balls[j].dy,balls[i].dy
    collision = new_collision

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    time.sleep(0.005)
#
pygame.quit()
