# Advent of Code

[Advent of Code](https://adventofcode.com) est une série d'exercices de programmation publiés chaque jour à 6h jusqu'au 25 décembre.

Ce sont d'excellents entraînements de programmation, de difficulté raisonnable au début et qui augmente avec les jours.

Pour participer :

* Vous avez besoin d'un compte google ou github pour vous connecter.

* Vous pouvez traduire automatiquement la page (sous chrome par exemple) si vous avez du mal en anglais.

* Les données de chaque problème sont fournies sous forme de fichier. Enregistrez le fichier au même
  endroit où vous écrirez vos programmes python de solutions (un programme par problème).
  
----

## Lire un fichier en python
  
Si le fichier est stocké dans un endroit "accessible" (dans Thonny, le même
  endroit que le programme fonctionne) :
  ```python
  f = open("nomdufichier.txt")
  ```
  Puis pour obtenir le contenu, trois méthodes :
  ```python
  L = f.readlines()
  # L est une liste de chaînes de caractères, chacunes des lignes du fichier
  # Ou bien :
  a = f.readline()
  b = f.readline()
  # a est la première ligne de f, b la deuxième ligne
  # Ou bien :
  for line in f:
	  print(line)
	  # line est une ligne de f, et la boucle les parcourt toutes dans l'ordre
  ```
  
---

## Manipulations des chaînes de caractère utiles

* Les lignes quand on lit un fichier contiennent en général le caractère de retour
  à la ligne `\n` à la fin. Pour enlever les espaces ou retour à la ligne au début et à
  la fin d'une chaîne de caractère s :
  ```python
  >>> "  12  ".strip()
  '12'
  ```
* Pour couper une chaîne de caractère à chaque fois qu'on rencontre un motif (par exemple
  ici "->") et renvoyer la liste de toutes les chaînes obtenues :
  ```python
  >>> "a->b->12".split("->")
  ['a', 'b', '12']
  >>> "1,2,3,4".split(",")
  ['1', '2', '3', '4']
  ```
* Pour transformer une chaîne de caractères en la liste de ses caractères :
  ```python
  >>> list("bonjour")
  ['b', 'o', 'n', 'j', 'o', 'u', 'r']
  ```
* Transformer une liste de chaînes de caractères représentant des nombres en liste de nombres :
  ```python
  L = ["12","13","42"]
  for i in range(len(L)):
	  L[i] = int(L[i])
  print(L) # affiche [12,13,42]
  ```
  

