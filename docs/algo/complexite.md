# Complexité Algorithmique

Le temps d'exécution des programmes est un sujet fondamental en informatique. Mais il est compliqué de dire
des choses précises dessus parce qu'il dépend de nombreux paramètres, entre autres :

* le modèle de l'ordinateur
* le langage de programmation utilisé

C'est pour ça qu'on a inventé la notion de **complexité**, qui est moins précise mais ne dépend que de l'algorithme utilisé.
Pour ça, on utilise comme paramètre `n` la taille du problème que notre algorithme va résoudre, et on s'intéresse au nombre
d'opérations élémentaires `T(n)`.

Définition : on dit qu'un algorithme a une complexité `O(f(n))` (se lit "en grand O de `f(n)`") si pour n suffisamment grand
le nombre d'opérations `T(n)` est majoré par une constante fois `f(n)` : `T(n) <= C * f(n)`.

Conséquence : comme on a le droit à une constante multiplicatrice, les complexité `O(n)` et `O(2n)` sont les mêmes, donc en 
fait on a peu de classes de complexité, et on va lister les plus courantes, de la plus rapide à la plus lente :

| Complexité | Nom | Exemple d'Algorithme | Si on multiplie `n` par 10, le temps est...|
|---|---|---|---|
|`O(1)`|constante|ajouter un élément à la fin d'une liste avec `append`|le même|
|`O(log(n))`|logarithmique|recherche dichotomique dans une liste triée|augmenté d'une constante|
|`O(n)`|linéaire|maximum,minimum,moyenne d'une liste|multiplié par 10|
|`O(n*log(n))`|quasi-linéaire|tri fusion, tri en python avec `sort`|"un peu plus" que x10|
|`O(n**2)`|quadratique|tri par sélection, tri par insertion|multiplié par 100|
|`O(n**3)`|cubique||multiplié par 1000|
|`O(2**n)`|exponentielle|sac à dos|mis à la puissance 10|
|`O(n!)`|factorielle|voyageur de commerce|"encore pire" que puissance 10|
